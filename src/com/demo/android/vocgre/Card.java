package com.demo.android.vocgre;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Card extends Activity {

	private TextView viewWord;
	private TextView viewMeaning;
	private TextView viewPhonetic;
	private Button preCard;
	private Button nextCard;
	private ImageButton bStar;
    protected static final int MENU_CREATE=Menu.FIRST;	
    protected static final int MENU_EDIT=Menu.FIRST+1;
    protected static final int MENU_DELETE=Menu.FIRST+2;
    protected static final int MENU_VOCAB=Menu.FIRST+3;
    private static DB mDbHelper;
    private static Cursor allCursor;
    private static long mRowId;
    private static int allPosition;
    public static final String PREF="CARD_PREF";
    public static final String PREF_POSITION="CARD_POSITION";
    public static final String PREF_SORTBYWHAT="SORTBYWHAT";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card);
        restorePrefs();
        
        mDbHelper=new DB(this);
        mDbHelper.open();
        allCursor=mDbHelper.getAll();
        startManagingCursor(allCursor);
//        Log.e(this.toString(), "The row before a jump is " +allCursor.getPosition());
        findViews();
        Log.e(this.toString(), "before showWord");
        
        showWord();
        Log.e(this.toString(), "before setListeners");
        setListeners();
    }

    private void findViews(){
    	viewWord=(TextView) findViewById(R.id.view_word);
    	viewPhonetic=(TextView) findViewById(R.id.view_phonetic);
    	viewMeaning=(TextView) findViewById(R.id.view_meaning);
    	preCard=(Button) findViewById(R.id.previous_card);
    	nextCard=(Button) findViewById(R.id.next_card);
//    	Log.e(this.toString(), "before bStar");
    	bStar=(ImageButton) findViewById(R.id.B_Star);
    }

    private void restorePrefs(){
    	SharedPreferences settings=getSharedPreferences(PREF, 0);
    	int pref_position=settings.getInt(PREF_POSITION, 0);
    	if(pref_position!=0){
    		allPosition=pref_position;
    	}
    	
    	int pref_sortbywhat=settings.getInt(PREF_SORTBYWHAT, 0);
        GlobalVariable globalVariable= (GlobalVariable) getApplicationContext();
    	globalVariable.nSortByWhat=pref_sortbywhat;
    }
    
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		SharedPreferences settings=getSharedPreferences(PREF, 0);
		settings.edit().putInt(PREF_POSITION, allPosition).commit();
	}

	// Present result
    private void showWord(){    	
		Bundle extras=getIntent().getExtras();
		int intent_position=(extras!=null?extras.getInt("ALL_POSITION"):allPosition);
		allPosition=intent_position;
//		Log.e(this.toString(), "before populateCard "+mRowId);
		populateCard(allPosition);	
//		Log.e(this.toString(), "after populateCard");
    }
    
	private void populateCard(int position){
//        Log.e(this.toString(),"before get Cursor");
    	allCursor.moveToPosition(position);
    	Log.e(this.toString(), "moveToPosition " +allCursor.getPosition());
//		Cursor pointedCursor=mDbHelper.get(mRowId);
		//      Log.e(this.toString(),"after get Cursor");
//		startManagingCursor(pointedCursor);
//		Log.e(this.toString(), "before setText");
		if(allCursor.getCount()!=0){
			if(!(allCursor.isAfterLast()||allCursor.isBeforeFirst())){
				showCard(allCursor.getPosition());
//				showMark(position);
			}
			else{
				showBlankCard();
			}
		}
		else{
			showBlankCard();
		}
	}
	
    private void showCard(int position){
    	allCursor.moveToPosition(position);
    	viewWord.setText(allCursor.getString(allCursor.getColumnIndex(DB.KEY_WORD)));
    	viewPhonetic.setVisibility(View.VISIBLE);
    	viewPhonetic.setText(allCursor.getString(allCursor.getColumnIndex(DB.KEY_PHONETIC)));
    	viewMeaning.setVisibility(View.INVISIBLE);
    	viewMeaning.setText(allCursor.getString(allCursor.getColumnIndex(DB.KEY_MEANING)));
    	viewMeaning.setMovementMethod(new ScrollingMovementMethod());
//		Log.e(this.toString(), "before get mark");
//		int mark=allCursor.getInt(allCursor.getColumnIndex(DB.KEY_MARK));
//		Log.e(this.toString(), "after get mark = "+mark);
		int mark=allCursor.getInt(allCursor.getColumnIndex(DB.KEY_MARK));
		showMark(mark);
    }

	private void showMark(int mark){
//		CharSequence contentDescription = bStar.getContentDescription();
		boolean bmark=(mark!=0);
		if(bmark==true){
			bStar.setBackgroundResource(R.drawable.btn_rating_star_on_selected);
		}
		else{
			bStar.setBackgroundResource(R.drawable.btn_rating_star_off_normal);
		}
		Log.e(this.toString(), "bmark = " +bmark);
	}

    private void setListeners(){
    	viewWord.setOnClickListener(answer);
    	viewPhonetic.setOnClickListener(answer);
    	preCard.setOnClickListener(preC);
    	nextCard.setOnClickListener(nextC);
    	bStar.setOnClickListener(B_markStar);
    }
  
    private OnClickListener answer=new OnClickListener()
    {
    	public void onClick(View v)
 		{
    		viewMeaning.setVisibility(View.VISIBLE);
    	}	
    };
    
    private OnClickListener preC=new OnClickListener()
    {
    	public void onClick(View v)
 		{
    		allCursor.moveToPosition(allPosition);
			Log.e(this.toString(), "The row before a jump is " +allCursor.getPosition());
    		if(allCursor.getCount()!=0){
    			if(allCursor.isFirst()){
    				allCursor.moveToPrevious();
    				showBlankCard();
    			}
    			else if(allCursor.isBeforeFirst()){
    				allCursor.moveToLast();
    				showCard(allCursor.getPosition());
    			}
    			else{
    				allCursor.moveToPrevious();
    				showCard(allCursor.getPosition());
    			}
    		}
    		allPosition=allCursor.getPosition();
    		Log.e(this.toString(), "The row after a jump is " +allCursor.getPosition());
//    		long cycRowId=(++revRowId)%(mDbHelper.getAll().getCount()+1);
//    		mRowId=((mDbHelper.getAll().getCount()+1)-(cycRowId))%(mDbHelper.getAll().getCount()+1);
//    		populateCard(mRowId);
    	}	
    };

    private OnClickListener nextC=new OnClickListener()
    {
    	public void onClick(View v)
 		{
    		allCursor.moveToPosition(allPosition);
			Log.e(this.toString(), "The row before a jump is " +allCursor.getPosition());
    		if(allCursor.getCount()!=0){
    			if(allCursor.isLast()){
    				allCursor.moveToNext();
    				showBlankCard();
    			}
    			else if(allCursor.isAfterLast()){
    				allCursor.moveToFirst();
    				showCard(allCursor.getPosition());
    			}
    			else{
    				allCursor.moveToNext();
    				showCard(allCursor.getPosition());
    			}
    		}
    		allPosition=allCursor.getPosition();
    		Log.e(this.toString(), "The row after a jump is " +allCursor.getPosition());
//    		mRowId=(++mRowId)%(mDbHelper.getAll().getCount()+1);
//    		populateCard(mRowId);
    	}	
    };
    
    private OnClickListener B_markStar=new OnClickListener()
    {
    	public void onClick(View v)
 		{
    		if(!(allCursor.isAfterLast()||allCursor.isBeforeFirst())){
	    		int mark=allCursor.getInt(allCursor.getColumnIndex(DB.KEY_MARK));
	    		Log.e(this.toString(), "before press star = "+ mark);
	    		mRowId=allCursor.getLong(allCursor.getColumnIndexOrThrow(DB.KEY_ROWID));
	    		mDbHelper.update(mRowId, 
	    				allCursor.getString(allCursor.getColumnIndex(DB.KEY_WORD)),
	    				allCursor.getString(allCursor.getColumnIndex(DB.KEY_PHONETIC)),
	    				allCursor.getString(allCursor.getColumnIndex(DB.KEY_MEANING)),
	    				(mark+1)%2);
	    		allPosition=allCursor.getPosition();
	    		allCursor=mDbHelper.getAll();
	    		allCursor.moveToPosition(allPosition);
	    		mark=allCursor.getInt(allCursor.getColumnIndex(DB.KEY_MARK));
	    		Log.e(this.toString(), "after press star = "+ mark);
	    		showMark(mark);
    		}
 		}
    };
    
    private void showBlankCard(){
		viewWord.setText(R.string.word1);
		viewPhonetic.setVisibility(View.INVISIBLE);
		viewMeaning.setVisibility(View.VISIBLE);
		viewMeaning.setText(R.string.meaning1);
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.add(0, MENU_CREATE, 0, "�s�W");
//		.setIcon(android.R.drawable.ic_menu_help);
    	menu.add(0, MENU_EDIT, 0, "�s��");
//    	.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
    	menu.add(0, MENU_DELETE, 0, "�R��");
    	menu.add(0, MENU_VOCAB, 0, "�r�J�w");

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
//		if(allCursor.getCount()==0){
//			mRowId=1;
//		}
//		else{
//			mRowId=allCursor.getLong(allCursor.getColumnIndexOrThrow(DB.KEY_ROWID));	
//		}
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case MENU_CREATE:
			Log.e(this.toString(), "allPosition is " + allPosition);
    		Intent intent0=new Intent(this, WordEdit.class);
        	intent0.putExtra(DB.KEY_ROWID, mDbHelper.create("","",""));
        	intent0.putExtra("ALL_POSITION", allPosition);
        	startActivity(intent0);
			break;
		case MENU_EDIT:
			if(allCursor.getCount()!=0){
				if(!(allCursor.isAfterLast()||allCursor.isBeforeFirst())){
					mRowId=allCursor.getLong(allCursor.getColumnIndexOrThrow(DB.KEY_ROWID));
					Intent intent1=new Intent(this, WordEdit.class);
					intent1.putExtra(DB.KEY_ROWID, mRowId);
					intent1.putExtra("ALL_POSITION", allPosition);
					startActivity(intent1);
				}
			}
    		break;
    	case MENU_DELETE:
			if(allCursor.getCount()!=0){
				if(!(allCursor.isAfterLast()||allCursor.isBeforeFirst())){
					mRowId=allCursor.getLong(allCursor.getColumnIndexOrThrow(DB.KEY_ROWID));
					allPosition=allCursor.getPosition();
					mDbHelper.delete(mRowId);
					--allPosition;
					mDbHelper.close();
			        mDbHelper.open();
			        Log.e(this.toString(), "after reopen DB");
			        allCursor=mDbHelper.getAll();
			        Log.e(this.toString(), "after getall DB");
//			        allCursor.moveToPosition(allPosition);
			        showWord();
				}
			}
    		break;
    	case MENU_VOCAB:
//    		allPosition=allCursor.getPosition();
    		Intent intent2=new Intent(this, Vocabulary.class);
        	intent2.putExtra("ALL_POSITION", allPosition);
        	startActivity(intent2);
    		break;
    	}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK){
//			Log.e(this.toString(), "brfore send intent_position " + allPosition);
//			allPosition=allCursor.getPosition();
			Log.e(this.toString(), "befort send intent_position " + allPosition);
    		Intent intent2=new Intent(this, Vocabulary.class);
        	intent2.putExtra("ALL_POSITION", allPosition);
        	startActivity(intent2);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}

package com.demo.android.vocgre;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DB{
	private static final String DATABASE_NAME="vocs.db";
	private static final int DATABASE_VERSION=1;
	private static final String DATABASE_TABLE="vocs";
	private static final String DATABASE_CREATE=
			"Create table vocs("
			+"[_id] INTEGER PRIMARY KEY,"
			+"[word] TEXT UNIQUE ON CONFLICT ABORT,"
			+"[phonetic] TEXT,"
			+"[meaning] TEXT,"
			+"[created] INTEGER,"
			+"[modified] INTEGER"
			+");";
//	private static String tDBPath = String.format("%s//%s", android.os.Environment.getExternalStorageDirectory().getAbsolutePath(), DATABASE_NAME);
//	private static File f = new File(tDBPath);
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	private Context mCtx=null;
	
	private static class DatabaseHelper extends SDSQLiteOpenHelper{
		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXITS " +DATABASE_TABLE);
			onCreate(db);
		}
	}
	
	/** Constructor */
	public DB(Context ctx){
		this.mCtx=ctx;
	}
	
	public DB open() throws SQLException{
		dbHelper=new DatabaseHelper(mCtx);
		db=dbHelper.getWritableDatabase();
//	    Log.e(this.toString(), "after open");
		return this;
	}
	
	public void close(){
		Log.e(this.toString(), "before close");
		dbHelper.close();
	}
	
	public static final String KEY_ROWID="_id";
	public static final String KEY_WORD="word";
	public static final String KEY_PHONETIC="phonetic";
	public static final String KEY_MEANING="meaning";
	public static final String KEY_CREATED="created";
	public static final String KEY_MARK="modified";
	String[] strCols=new String[]{
			KEY_ROWID,
			KEY_WORD,
			KEY_PHONETIC,
			KEY_MEANING,
			KEY_CREATED,
			KEY_MARK
	};
	
	// get all entries
    public Cursor getAll(){
    	GlobalVariable globalVariable= (GlobalVariable) this.mCtx.getApplicationContext();
    	int nSBWhat=globalVariable.nSortByWhat;
    	String SBWhat=null;
    	switch (nSBWhat){
    	case 0:
    		SBWhat=KEY_ROWID;
    		break;
    	case 1:
    		SBWhat=KEY_WORD;
    		break;
    	case 2:
    		SBWhat=KEY_MARK;
    		break;
    	}
    	
    	Log.e(this.toString(), "Sort all by " +SBWhat);
 //   	Log.e(this.toString(), "sortByWhat " +sortByWhat);
    	Cursor mCursor=db.query(DATABASE_TABLE, // Which table to select
    			strCols,// Which columns to return
    			null, // WHERE clause
    			null, // WHERE arguments
    			null, // GROUP BY clause
    			null, // HAVING clause
    			SBWhat // Order-by clause
    			);
    	if (mCursor!=null){
    		mCursor.moveToFirst();
    	}
    	return mCursor;
    }

    // add an entry
    public long create(String Word, String Phonetic, String Meaning){
    	Date now=new Date();
    	ContentValues args=new ContentValues();
    	args.put(KEY_WORD, Word);
    	args.put(KEY_PHONETIC, Phonetic);
    	args.put(KEY_MEANING, Meaning);
    	args.put(KEY_CREATED, now.getTime());
    	args.put(KEY_MARK, 0);
    	return db.insert(DATABASE_TABLE, null, args);
    }
    
    // remove an entry
    public boolean delete(long rowId){
    	return (db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) >0);
    }
    
    // query single entry
    public Cursor get(long rowId) throws SQLException{
//	    Log.e(this.toString(), "brfore query in DB");
    	Cursor mCursor=db.query(true,
    			DATABASE_TABLE,
    			strCols,
    			KEY_ROWID+ "=" + rowId,
    			null, null, null, KEY_WORD, null);
	    Log.e(this.toString(), "after get in DB position is" +mCursor.getPosition());

    	if (mCursor!=null){
    		mCursor.moveToFirst();
    	}
    	
    	return mCursor;
    }
    
    public Cursor wget(String Arg) throws SQLException{
	    Log.e(this.toString(), "brfore query word in DB");
	    String[] selectionArgs=new String[]{Arg};
    	Cursor mCursor=db.query(true,
    			DATABASE_TABLE,
    			strCols,
    			KEY_WORD+ "=?" ,
    			selectionArgs,
    			null, null, KEY_WORD, null);
	    Log.e(this.toString(), "after query word in DB");
//	    Log.e(this.toString(), mCursor.toString());

    	if (mCursor!=null){
    		mCursor.moveToFirst();
    	}
    	
    	return mCursor;
    }
    
    public String[] getAllWords()
    {
//    	Log.e(this.toString(), "before query word in DB");
    	Cursor cursor=db.query(DATABASE_TABLE, // Which table to select
    			new String[] {KEY_WORD},// Which columns to return
    			null, // WHERE clause
    			null, // WHERE arguments
    			null, // GROUP BY clause
    			null, // HAVING clause
    			KEY_WORD // Order-by clause
    			);
//        Log.e(this.toString(), "after query word in DB");
        if(cursor.getCount() >0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(KEY_WORD));
                 i++;
             }
            return str;
        }
        else
        {
            return new String[] {};
        }
    }

    // update
    public boolean update(long rowId, String word, String phonetic, String meaning, int mark){
    	ContentValues args=new ContentValues();
    	args.put(KEY_WORD, word);
    	args.put(KEY_PHONETIC, phonetic);
    	args.put(KEY_MEANING, meaning);
    	args.put(KEY_MARK, mark);
    	return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) >0;
    }
}
package com.demo.android.vocgre;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class Options extends Activity {

	private Spinner Spinner_Sort;
	private ArrayAdapter<String> adapter;
	private static final String[] sortMethod={"存入順序", "字母順序", "星號標記"};
    public static final String PREF="CARD_PREF";
    public static final String PREF_SORTBYWHAT="SORTBYWHAT";
	private DB mDbHelper;
	private long mRowId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
//		Log.e(this.toString(), "Enter Options");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options);
        findViews();
        
		mDbHelper=new DB(this);
		mDbHelper.open();
		Bundle extras=getIntent().getExtras();
		mRowId=extras.getLong(DB.KEY_ROWID);
		Log.e(this.toString(), "Old position " + getAllPosition(mRowId));
		
//        restorePrefs();
//        Log.e(this.toString(), "Enter Options");

		adapter=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sortMethod);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner_Sort.setAdapter(adapter);
        Spinner_Sort.setOnItemSelectedListener(new SpinnerSelectedListener());
        Spinner_Sort.setVisibility(View.VISIBLE);

        GlobalVariable globalVariable= (GlobalVariable) getApplicationContext();
    	int nSBWhat=globalVariable.nSortByWhat;
        Spinner_Sort.setSelection(nSBWhat);
	}
	

    private void findViews(){
    	Spinner_Sort=(Spinner) findViewById(R.id.SpinnerSort);
    }
    

   
    class SpinnerSelectedListener implements OnItemSelectedListener{
    	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3){
            GlobalVariable globalVariable= (GlobalVariable) getApplicationContext();
        	globalVariable.nSortByWhat=arg2;
    	}
    	public void onNothingSelected(AdapterView<?> arg0){}
    }
    
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
    	GlobalVariable globalVariable= (GlobalVariable) getApplicationContext();
    	int nSBWhat=globalVariable.nSortByWhat;
		SharedPreferences settings=getSharedPreferences(PREF, 0);
		settings.edit().putInt(PREF_SORTBYWHAT, nSBWhat).commit();
	}
    
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		Intent intent=new Intent(this, Vocabulary.class);
		intent.putExtra("ALL_POSITION", getAllPosition(mRowId));
		Log.e(this.toString(), "New position " + getAllPosition(mRowId));
		startActivity(intent);
	}
	
    public int getAllPosition(long rowId){
    	mDbHelper.close();
    	mDbHelper.open();
    	Cursor mCursor=mDbHelper.getAll();
//    	Log.e(this.toString(), "Before having");
    	while(mCursor.getInt(mCursor.getColumnIndexOrThrow(DB.KEY_ROWID))!=rowId){
    		mCursor.moveToNext();
    	}
    	return mCursor.getPosition();
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK){
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
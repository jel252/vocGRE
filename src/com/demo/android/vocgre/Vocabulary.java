package com.demo.android.vocgre;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class Vocabulary extends ListActivity {
	private Button query_db;
	private TextView empty_db;
	private ListView voc_List;
	private AutoCompleteTextView search_word;
	private String[] word_list;
    private DB mDbHelper;
    private Cursor allCursor;
    private Cursor wgetCursor;
    private static long mRowId;
    private static int allPosition;
    protected static final int MENU_INSERT=Menu.FIRST;  
    protected static final int MENU_DELETE=Menu.FIRST+1;
    protected static final int MENU_EDIT=Menu.FIRST+2;
    protected static final int MENU_OPTIONS=Menu.FIRST+3;
	
	/** Called when the activity is first created.*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vocabulary);
        findViews();

		Bundle extras=getIntent().getExtras();
		int intent_position=(extras!=null?extras.getInt("ALL_POSITION"):0);
//        Log.e(this.toString(), "intent_position is " + intent_position);
		allPosition=intent_position;
        mDbHelper=new DB(this);
        mDbHelper.open();
        
//        Log.e(this.toString(), "brfore getAllWords");
        word_list=mDbHelper.getAllWords();
//        Log.e(this.toString(), "after getAllWords");
		ArrayAdapter<String> qsadapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,word_list);
		search_word.setAdapter(qsadapter);
		search_word.setThreshold(1);
        
        // Tell the list view which to display when the list is empty
        getListView().setEmptyView(empty_db);
        registerForContextMenu(getListView());
        setAdapter();
        voc_List.setSelection(intent_position);

        setListeners();
    }

    private void fillData(){
    	allCursor=mDbHelper.getAll();
    	startManagingCursor(allCursor);
    	
    	// Create an array to specify the fields we want to display in the list
    	String[] from=new String[]{DB.KEY_WORD, DB.KEY_MEANING};
    	// an array of the fields we want to bind those fields to
    	int[] to=new int[]{android.R.id.text1, android.R.id.text2};
    	
    	// Now create a simple cursor adapter
    	SimpleCursorAdapter adapter=
    			new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, allCursor, from, to);
    	setListAdapter(adapter);
    }
    
    private void setAdapter(){
    	mDbHelper=new DB(this);
    	mDbHelper.open();
    	fillData();
    }
        
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// TODO Auto-generated method stub
    	menu.add(0, MENU_INSERT, 0, "新增單字");
    	menu.add(0, MENU_OPTIONS, 0, "其他設定");
    	return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case MENU_INSERT:
    		Log.e(this.toString(), "allPosition is " + allCursor.getPosition());
    		Intent intent=new Intent(this, WordEdit.class);
        	intent.putExtra(DB.KEY_ROWID, mDbHelper.create("", "", ""));
        	intent.putExtra("ALL_POSITION", allCursor.getPosition());
        	startActivity(intent);
        	break;
    	case MENU_OPTIONS:
    		allCursor.moveToPosition(allPosition);
			mRowId=allCursor.getLong(allCursor.getColumnIndexOrThrow(DB.KEY_ROWID));
			Intent intent1=new Intent(this, Options.class);
			intent1.putExtra(DB.KEY_ROWID, mRowId);
    		Log.e(this.toString(), "Old allPosition is " + allCursor.getPosition());
        	startActivity(intent1);
        	break;
    	}

        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
    	super.onListItemClick(l, v, position, id);
		
		Intent intent=new Intent(this, Card.class);		
		intent.putExtra("ALL_POSITION", getAllPosition(id));
		startActivity(intent);
    }

    public int getAllPosition(long rowId){
    	Cursor mCursor=mDbHelper.getAll();
    	while(mCursor.getInt(mCursor.getColumnIndexOrThrow(DB.KEY_ROWID))!=rowId){
    		mCursor.moveToNext();
    	}
    	return mCursor.getPosition();
    }
    
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		menu.add(0, MENU_DELETE, 0, "刪除單字");
		menu.add(0, MENU_EDIT, 0, "編輯單字");
		menu.setHeaderTitle("要怎麼處理這個單字?");
		super.onCreateContextMenu(menu, v, menuInfo);
	}
    
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		AdapterView.AdapterContextMenuInfo info;
		info=(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		
		switch(item.getItemId()){
		case MENU_DELETE:
			mDbHelper.delete(info.id);
			fillData();
			break;
		case MENU_EDIT:
			Intent intent=new Intent(this, WordEdit.class);
			intent.putExtra(DB.KEY_ROWID, info.id);
			intent.putExtra("ALL_POSITION", allCursor.getPosition());
			startActivity(intent);
			break;

		}
		return super.onContextItemSelected(item);
	}
	
	private void findViews(){
		empty_db=(TextView) findViewById(R.id.empty);
		query_db=(Button) findViewById(R.id.querydb);
//		search_word=(EditText) findViewById(R.id.searchword);
		voc_List=(ListView) findViewById(android.R.id.list);
		search_word = (AutoCompleteTextView) findViewById(R.id.searchword);
	}
	
	private void setListeners(){
		query_db.setOnClickListener(search_wqet);
/*
        search_word.setOnItemClickListener(new OnItemClickListener(){ 
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
             for(int i=0;i<word_list.length;i++){
              if(search_word.getText().toString().equals(word_list[i].toString())==true){
            	  voc_List.setSelection(i);
              }              
             }             
            } 
        });
        */
	}
	
	private void search_word_in_db(){
		mDbHelper=new DB(this);
    	mDbHelper.open();
    	Log.e(this.toString(), "Before search " + search_word.getText().toString());
    	String searchWord=search_word.getText().toString();
    	if(!searchWord.equals("")){
    		wgetCursor=mDbHelper.wget(searchWord);
       		startManagingCursor(wgetCursor);
    	
    		// Create an array to specify the fields we want to display in the list
    		String[] from=new String[]{DB.KEY_WORD, DB.KEY_MEANING};
    		// an array of the fields we want to bind those fields to
    		int[] to=new int[]{android.R.id.text1, android.R.id.text2};
    	
    		// Now create a simple cursor adapter
    		SimpleCursorAdapter adapter2=
    				new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, wgetCursor,from, to);
    		setListAdapter(adapter2);
    		Log.e(this.toString(), "After query word");
    	}
    	else{
    		setAdapter();
    	}
	}
	
    private OnClickListener search_wqet=new OnClickListener()
    {
    	public void onClick(View v)
 		{
    		search_word_in_db();
    	}
    };

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK){
			Bundle extras=getIntent().getExtras();
			int allPosition=extras.getInt("ALL_POSITION");
			
			Intent intent=new Intent(this, Card.class);
			intent.putExtra("ALL_POSITION", allPosition);
			startActivity(intent);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
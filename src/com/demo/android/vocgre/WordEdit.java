package com.demo.android.vocgre;

import android.app.Activity;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WordEdit extends Activity{
	private DB mDbHelper;
	private Long mRowId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mDbHelper=new DB(this);
		mDbHelper.open();
		setContentView(R.layout.wordedit);
		findViews();
		showViews(savedInstanceState);
		setListeners();
	}
	
	
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		Intent intent=new Intent(this, Card.class);
		intent.putExtra("ALL_POSITION", getAllPosition(mRowId));
		startActivity(intent);
	}

	public void cancel(){
		Bundle extras=getIntent().getExtras();
		int allPosition=extras.getInt("ALL_POSITION");
		Intent intent=new Intent(this, Card.class);
		intent.putExtra("ALL_POSITION", allPosition);
		startActivity(intent);
	}
	
    public int getAllPosition(long rowId){
    	
    	Cursor mCursor=mDbHelper.getAll();
    	Log.e(this.toString(), "Before having");
    	while(mCursor.getInt(mCursor.getColumnIndexOrThrow(DB.KEY_ROWID))!=rowId){
    		mCursor.moveToNext();
    	}
    	return mCursor.getPosition();
    }

	private EditText field_note;
	private EditText field_meaning;
	private Button button_confirm;
	private Button search;
	private String input;
	private String match;
	private String phonetic;
	private String bk_note;
	private String bk_meaning;
	private int mark;

	
	private void findViews(){
		field_note=(EditText) findViewById(R.id.note);
		field_meaning=(EditText) findViewById(R.id.note_meaning); 
		button_confirm=(Button) findViewById(R.id.confirm);
		search = (Button) findViewById(R.id.search);
	}
	

	
	private void showViews(Bundle savedInstanceState){
		if(mRowId==null){
			Bundle extras=getIntent().getExtras();
			mRowId=extras!=null?extras.getLong(DB.KEY_ROWID):null;
//			allPosition=extras!=null?extras.getInt("ALL_POSITION"):0;
		}
		
		populateFields();
	}
	
	private void populateFields(){
		if(mRowId!=null){
			Cursor note=mDbHelper.get(mRowId);
			startManagingCursor(note);
			phonetic=note.getString(note.getColumnIndexOrThrow(DB.KEY_PHONETIC));
			field_note.setText(note.getString(note.getColumnIndexOrThrow(DB.KEY_WORD)));
			field_meaning.setText(note.getString(note.getColumnIndexOrThrow(DB.KEY_MEANING)));
			bk_note=note.getString(note.getColumnIndexOrThrow(DB.KEY_WORD));
			bk_meaning=note.getString(note.getColumnIndexOrThrow(DB.KEY_MEANING));
			mark=note.getInt(note.getColumnIndexOrThrow(DB.KEY_MARK));
		}
	}
	
	private void setListeners(){
		button_confirm.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view){
				try{
					if(!field_note.getText().toString().equals("")){
						mDbHelper.update(mRowId,
								field_note.getText().toString(), 
								phonetic, 
								field_meaning.getText().toString(), 
								mark);
						setResult(RESULT_OK);
						finish();
						}
					
					else{
						mDbHelper.delete(mRowId);
						setResult(RESULT_OK);
						cancel();
					}
				} catch (SQLiteException e){
					Toast.makeText(WordEdit.this, "��r������", Toast.LENGTH_SHORT).show();
					field_note.setText(bk_note);
					field_meaning.setText(bk_meaning);					
				}
			}
		});
		search.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view){
				// TODO Auto-generated method stub  
                input = field_note.getText().toString();  
                XmlResourceParser xrp = getResources().getXml(R.xml.cet4);
  
                try {  
                    StringBuilder strbuilder = new StringBuilder("");
                    StringBuilder phstrbuilder = new StringBuilder(""); 
                    while (xrp.getEventType() != XmlResourceParser.END_DOCUMENT) { 
                        if (xrp.getEventType() == XmlResourceParser.START_TAG) {  
                            String tagname = xrp.getName();
                            
                            if (tagname.equals("word")) {  
                                match = xrp.nextText();
  
                                if (match.equals(input)) {
                                    xrp.next();
                                    strbuilder.append(xrp.nextText());
                                    field_meaning.setText(strbuilder);
                                    xrp.next();
                                    phstrbuilder.append(xrp.nextText());
//                                    phonetic="0";
                                    phonetic=(phstrbuilder.toString()!=""?phstrbuilder.toString():phonetic);
                                    break;  
                                }
                            }

  
                        }  
                        xrp.next();  
  
                    }  
  
                } catch (Exception e) {  
                    // TODO: handle exception  
                }
			}
			
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(!bk_note.equals("")){
			if(keyCode==KeyEvent.KEYCODE_BACK){
				field_note.setText(bk_note);
				field_meaning.setText(bk_meaning);
				cancel();
			}
		}
		else{
			if(keyCode==KeyEvent.KEYCODE_BACK){
				mDbHelper.delete(mRowId);
				cancel();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}